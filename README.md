# oltspecs



## Primeira Release
Sistema de Monitoramente e Gerenciamento para Provedores



### Bilhetagem
  Pagamento trismestral, semestral ou anual <br />
  Valor Atual: $75.00, $150.00 ,$300.00 <br />
  Necessario definir a regra de negocio para este ponto, gateway de pagamento e afins.
  

### LOGS
- [ ] Utilização geral de usuarios


### OLT
- [ ] Temperatura e UPTime
- [ ] Trazer os Cards da OLT
- [ ] Pon Ports
- [ ] UpLink
- [ ] Vlan
- [ ] Mgmt IPs
- [ ] Remove ACLs
- [ ] Voip Profiles
- [ ] Advanced'
````
    OLT Setting	Value
    Show admin-disabled ONUs in Unconfigured	  boolean
    Use DHCP Option82	                          boolean
    DHCP Option82 field	
    Use PPPoE-plus	                              boolean
    ONU IP DHCP-Snooping	                      disabled | active
  ONU IP source-guard	                          disabled | active
    Use max-mac-learn	                          boolean
    Max MACs allowed on internet vport per ONU	  number
    Use MAC allow list	                          boolean
    Use MAC drop list	                          boolean
    Use Ports filtering ACL	                      boolean
    Ports filtering ACL	
    ONUs signal level "warning" (dBm)	          number 
    ONUs signal level "critical" (dBm)	          number
    Separate IP-hosts for VoIP/Mgmt	              boolean
    OLT temperature level "warning" (℃)	      number
    OLT temperature level "critical" (℃)	      number
    Use CVLAN-ID option	                          boolean
    Use SVLAN-ID option	                          boolean
    Use "other-all" TLS VLAN	                  boolean
````
### ONU
- [ ] Criação de Perfis para as ONUS
  
  #### Visualização 
  - [ ] Traffic/Signal ( por periodo de tempo )
  - [ ] DBM ( por periodo de tempo )
  
  #### Ações
  - [ ] Reboot
  - [ ] Resync
  - [ ] Restore
  - [ ] Disable
  - [ ] Delete
  - [ ] VOIP
  - [ ] CATV
  - [ ] Get Status
  - [ ] Show Running-Config
  - [ ] SW Info
  - [ ] Live ( real time ) 
  - [ ] Speed Profile
  - [ ] Ethernet Ports


  - [ ] Criação de Perfil para as ONUS: 

  ```
    ONUType = { 
      Pon Type: "GPON" | "EPON"
      Onu Type: string;
      Ethernet ports: number;
      WIFI SSIDs: number
      VoIP ports: number;
      CATV: boolean;
      Allow custom profiles: boolean
      Default Custom Profile
      Capability: Bridging | Bridging/Routing
    }
    ```


### ODB
- [ ] Integração com maps para trazer a geolocalização

### Chamados
  Sistema completo de chamados/ordem de serviço (OS)

### Relatorios
  Relatorios para as OS


### Alertas
  - [ ] Whatsapp
  - [ ] Discord
  - [ ] Telegram
  - [ ] SMS
  - [ ] Email

### Sistema de Permissionamento
  Importante para trazer e possibilitar quais funcionarios dentro do provedor desde o tecnico ao dono, para realização de serviços e manutenções.


